package com.ranull.petting.events;

import com.ranull.petting.Petting;
import com.ranull.petting.manager.PettingManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class Events implements Listener {
    private Petting plugin;
    private PettingManager pettingManager;

    public Events(Petting plugin, PettingManager pettingManager) {
        this.plugin = plugin;
        this.pettingManager = pettingManager;
    }

    @EventHandler
    public void onEntityInteract(PlayerInteractEntityEvent event) {
        if (!event.getPlayer().hasPermission("petting.use")) {
            return;
        }

        if (plugin.getConfig().getBoolean("settings.shift")) {
            if (!event.getPlayer().isSneaking()) {
                return;
            }
        } else {
            if (event.getPlayer().isSneaking()) {
                return;
            }
        }

        if (plugin.getConfig().getBoolean("settings.hand")
                && !event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)) {
            return;
        }

        if (plugin.getConfig().getBoolean("settings.cancelEvent")) {
            event.setCancelled(true);
        }

        if (event.getHand().equals(EquipmentSlot.HAND)) {
            pettingManager.petEntity(event.getRightClicked(), event.getPlayer());
        }
    }
}
