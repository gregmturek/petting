package com.ranull.petting;

import com.ranull.petting.commands.PettingCommand;
import com.ranull.petting.events.Events;
import com.ranull.petting.manager.PettingManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Petting extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        PettingManager pettingManager = new PettingManager(this);

        getServer().getPluginManager().registerEvents(new Events(this, pettingManager), this);
        getCommand("petting").setExecutor(new PettingCommand(this));
    }
}
