package com.ranull.petting.animation;

import com.ranull.petting.Petting;
import org.bukkit.entity.Player;

public class Animation {
    private Petting plugin;

    public Animation(Petting plugin) {
        this.plugin = plugin;
    }

    public void handAnimation(Player player) {
        if (plugin.getServer().getVersion().contains("1.13")) {
            new Animation113().handAnimation(player);
        } else if (plugin.getServer().getVersion().contains("1.14")) {
            new Animation114().handAnimation(player);
        } else if (plugin.getServer().getVersion().contains("1.15")) {
            new Animation115().handAnimation(player);
        } else if (plugin.getServer().getVersion().contains("1.16")) {
            new Animation116().handAnimation(player);
        }
    }
}