package com.ranull.petting.manager;

import com.ranull.petting.Petting;
import com.ranull.petting.animation.Animation;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class PettingManager {
    private Petting plugin;
    private ConcurrentMap<UUID, Long> petMap = new ConcurrentHashMap<>();

    public PettingManager(Petting plugin) {
        this.plugin = plugin;
    }

    public void petEntity(Entity entity, Player player) {
        if (canPet(entity)) {
            if (plugin.getConfig().isSet("settings.entities." + entity.getType().toString())) {
                if (entity instanceof Tameable) {
                    Tameable tameable = (Tameable) entity;

                    if (plugin.getConfig().getBoolean("settings.tamed")) {
                        if (!tameable.isTamed()) {
                            return;
                        }
                    }

                    if (plugin.getConfig().getBoolean("settings.owner")) {
                        if (!tameable.getOwner().equals(player)) {
                            return;
                        }
                    }
                }

                playSound(entity);
                spawnParticle(entity);
                healEntity(entity);

                new Animation(plugin).handAnimation(player);
            }
        }
    }

    private Boolean canPet(Entity entity) {
        if (petMap.containsKey(entity.getUniqueId())) {
            if ((System.currentTimeMillis() - petMap.get(entity.getUniqueId())) >
                    (plugin.getConfig().getDouble("settings.petDelay") * 1000)) {
                petMap.remove(entity.getUniqueId());
            }
        } else {
            petMap.put(entity.getUniqueId(), System.currentTimeMillis());
            return true;
        }
        return false;
    }

    private void healEntity(Entity entity) {
        if (entity instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) entity;

            double maxHealth = livingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
            double healAmount = plugin.getConfig().getDouble("settings.healAmount");

            if ((livingEntity.getHealth() + healAmount) > maxHealth) {
                livingEntity.setHealth(maxHealth);
            } else {
                livingEntity.setHealth(((LivingEntity) entity).getHealth() + healAmount);
            }
        }
    }

    private void playSound(Entity entity) {
        Sound sound = Sound.valueOf(plugin.getConfig().getString("settings.entities."
                + entity.getType().toString().toUpperCase()));
        if (sound != null) {
            float volume = (float) plugin.getConfig().getDouble("settings.volume");
            float pitch = (float) plugin.getConfig().getDouble("settings.pitch");

            entity.getWorld().playSound(entity.getLocation(), sound, volume, pitch);
        }
    }

    private void spawnParticle(Entity entity) {
        entity.getWorld().spawnParticle(Particle.HEART, entity.getLocation().clone().add(0, 1, 0), 1);
    }
}
