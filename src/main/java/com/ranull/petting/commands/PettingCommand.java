package com.ranull.petting.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class PettingCommand implements CommandExecutor {
    private com.ranull.petting.Petting plugin;

    public PettingCommand(com.ranull.petting.Petting plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.2.1";
        String author = "Ranull";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.YELLOW + "Petting " + ChatColor.GRAY
                    + ChatColor.GRAY + "v" + version);
            if (sender.hasPermission("petting.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/petting reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1) {
            if (args[0].toLowerCase().equals("reload")) {
                if (sender.hasPermission("petting.reload")) {
                    plugin.reloadConfig();
                    sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.YELLOW + "Petting"
                            + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Config reloaded!");
                } else {
                    sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.YELLOW + "Petting"
                            + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " No Permission!");
                }
            }
        }
        return true;
    }
}